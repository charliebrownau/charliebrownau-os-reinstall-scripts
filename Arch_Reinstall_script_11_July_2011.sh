#!/bin/bash


# Arch_Reinstall_Script
# by Charliebrownau
# charliebrownau@protonmail.com
# https://creation.social/u/charliebrownau
# joshwhotv.com/channel/charliebrownau
# 11-July-2021

# defined
VER="11_July_2021"

# Tasks
# -------
# Display free mem and storage space
# Update Repo list
# Update System repo
# Remove Software from repo
# Install Software from repo
# Display free mem and storage space
# download appimages

# ---------------------------------------
 
# display welcome text
# Clear
	echo "Charliebrownau Reinstall Script"
 	echo "  " 
 	echo "${VER}"


# ask user to continue
# 	read -p "Shall We Continue - Y/N" ANSWER
# 	case "$ANSWER" in
# 	[yY] [yes] [YES] [Yes]
#	 	install_arch
# 				;;
# 	[Nn] [No] [NO]
# 		goodbye
# 				;;



# install stuff

#  install_arch

	df -h
	free -h


 	sudo pacman -Scc
 	sudo pacman -Syu

 	sudo pacman -R firefox
 	sudo pacman -R vlc
 	sudo pacman -R libreoffice 

 	sudo pacman -S smplayer 

 	sudo pacman -S htop nano 
 	sudo pacman -S wget youtube-dl putty gftp
 	sudo pacman -S mumble hexchat dino
 	sudo pacman -S shortcut simplescreenrecorder audacity
 	sudo pacman -S mtpaint gnumeric abiword mupdf xed
	sudo pacman -S volumeicon vorbis-tools mpg123
	sudo pacman -S newsboat claws-mail gparted keepassXC

	df -h
	free -h


# download appimages into downlaods folder
	cd home
	cd Downloads
	mkdir appimage
 	cd appimage

 	wget https://gitlab.com/librewolf-community/browser/appimage/-/jobs/1374407522/artifacts/raw/LibreWolf-89.0.2-1.x86_64.AppImage

	wget https://libreoffice.soluzioniopen.com/stable/fresh/LibreOffice-fresh.basic-x86_64.AppImage

	wget https://github.com/skyjake/lagrange/releases/download/v1.5.2/Lagrange-1.5.2-x86_64.AppImage

	wget https://github.com/mltframework/shotcut/releases/download/v21.03.21/shotcut-linux-x86_64-210321.AppImage



# finish program display msg and leave 
# goodbye
 echo Thanks for using Charliebrownau Resinstall Script
 
