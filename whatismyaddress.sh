#!/bin/bash

# whatismyipaddress script
# by charliebrownau
# charliebrownau@protonmail.com
# 10 July 2021

# ------------------------

echo "$(ifconfig -a | grep broadcast | awk '{print $2}')"