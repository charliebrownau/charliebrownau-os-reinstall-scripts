
#!/bin/bash


# Debian_Reinstall_Script
# by Charliebrownau
# charliebrownau@protonmail.com
# https://creation.social/u/charliebrownau
# joshwhotv.com/channel/charliebrownau
# 11-July-2021


# defined
VER="11_July_2021"
OS="DEBIAN"


# Tasks
# -------
# Display free mem and storage space
# Update Repo list
# Update System repo
# Remove Software from repo
# Install Software from repo
# Display free mem and storage space
# download appimages

# ---------------------------------------
 
# display welcome text
# Clear
	echo "Charliebrownau Reinstall Script - for "
 	echo "${OS}"
	echo " " 
 	echo "${VER}"


# ask user to continue
# 	read -p "Shall We Continue - Y/N" ANSWER
# 	case "$ANSWER" in
# 	[yY] [yes] [YES] [Yes]
#	 	install_debian
# 				;;
# 	[Nn] [No] [NO]
# 		goodbye
# 				;;



# install stuff

#  install_debian

	df -h
	free -h

 	sudo apt update
	sudo apt upgrade

 	sudo apt remove firefox
 	sudo apt remove vlc
 	sudo apt remove libreoffice 

 	sudo apt install smplayer 
 	sudo apt install htop nano 
 	sudo apt install wget youtube-dl putty gftp
 	sudo apt install mumble hexchat dino
 	sudo apt install shortcut simplescreenrecorder audacity
 	sudo apt install mtpaint gnumeric abiword mupdf xed
	sudo apt install volumeicon vorbis-tools mpg123
	sudo apt install newsboat claws-mail gparted keepassXC

	df -h
	free -h


# download appimages into downlaods folder
	cd home
	cd Downloads
	mkdir appimage
 	cd appimage

 	wget https://gitlab.com/librewolf-community/browser/appimage/-/jobs/1374407528/artifacts/raw/LibreWolf-89.0.2-1.x86_64.AppImage

	wget https://libreoffice.soluzioniopen.com/stable/fresh/LibreOffice-fresh.basic-x86_64.AppImage

	wget https://github.com/skyjake/lagrange/releases/download/v1.5.2/Lagrange-1.5.2-x86_64.AppImage

	wget https://github.com/mltframework/shotcut/releases/download/v21.03.21/shotcut-linux-x86_64-210321.AppImage



# finish program display msg and leave 
# goodbye
 echo Thanks for using Charliebrownau Resinstall Script
 
